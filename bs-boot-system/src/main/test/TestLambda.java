
import com.google.common.base.Function;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @author Swimming Dragon
 * @description: TODO
 * @date 2023年12月28日 15:42
 */
public class TestLambda{
public static void main(String[] args){

    Function<User, String> func = User::func;
    System.out.println();

}
    
}

class User{
    private String name;
    public String func(){
        return this.name;
    }
}